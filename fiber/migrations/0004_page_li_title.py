# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fiber', '0003_page_is_visible_in_session'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='li_title',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Li title', choices=[(b'username', 'Username'), (b'full_name', 'Full name')]),
            preserve_default=True,
        ),
    ]
