# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fiber', '0002_page_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='is_visible_in_session',
            field=models.BooleanField(default=True, verbose_name=b'Is visible in session'),
            preserve_default=True,
        ),
    ]
