# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('fiber', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='group',
            field=models.ManyToManyField(to='auth.Group', verbose_name='groups', blank=True),
            preserve_default=True,
        ),
    ]
